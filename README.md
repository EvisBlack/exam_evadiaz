ORDINARY EXAM 1ST TERM ONLY EVA DIAZ PEREZ
============================================

First I changed server Ip from dinamyc Ip to static:
	- I change with GUI
	- Control center > Internet and Network > Advanced Network Configuraction
	- Ethernet > Wired connection 1 > IPv4 Settings
	- Method > Manual
	- Address 10.0.2.5 netmask 255.255.255.0	gateway 10.0.2.1
	- DNS servers 10.0.2.5 


1. FTP server on port 2022:
	In config file /etc/proftpd/modules.conf we go to configure our virtual host with this configuration, for that write tag <VirtualHost 10.0.2.5>
		+ For port number use: Port 2022
	> Users will be jailed at directory /var/www/html
		+ DefaultRoot /var/www/html
	> except by users on root group, those are jailed on their home
		+ DefaultRoot ~ root
	
	> start connection on the Desktop directory
		+ DefaultChdir ~/Desktop/ root
		
	> Anonymous connections will be allowed
		The Anonymous configuration will be between  <Anonymous ~ftp></Anonymous> tags
	> jailed in path “/var/www/html” too
		+ DefaultRoot /var/www/html
	> They have write permissions there:
		<Directory /var/www/html>
     			<Limit WRITE>
       			Allow
     			</Limit>
   		</Directory>
	> but not on the folder “/var/www/html/private”
		<Directory /var/www/html/private>
     			<Limit WRITE>
       			DenyAll
     			</Limit>
   		</Directory>
	> Connections on port 21 are only allowed by your main user
		This configuration is in /etc/proftpd.conf
		I supposse that the main user in virtual machine is osboxes
		And add the following lines in this file:
			<limit LOGIN>
				DenyAll
    				Allow user osboxes #main user
			<limit>


2. Configure a Server 

	> The default host will contain a PHP Directory Management System
		Install php and mysql:
			sudo apt-get install mysql-server php libapache2-mod-php php-mysql
		Enable  php module
			sudo a2enmod php 8.1
		Start mysql and we are going to change autentification method for root user without password
		sudo service mysql start
		sudo mysql
		
		mysql> USE mysql
		mysql> UPDATE user SET plugin='mysql_native_password' WHERE User='root';
		mysql> FLUSH PRIVILEGES;
		mysql> exit
		
		sudo service mysql restart
	> And now we are going to move application files to var/www/html
		sudo mv /home/osboxes/DirectoryManagementSystem/dms var/www/html/dms
	> Change folder permissions
		sudo chown -R www-data /var/www/html/dms
	> Now we are going to create database
		mysql -u root
		mysql> CREATE DATABASE loginsystem;
		mysql> USE loginsystem
		mysql> source /home/osboxes/DirectoryManagementSystem/SqlFile/dmsdb.sql
		mysql> exit;
	> For localhost show our index.php we have to change this configuration file:
		/etc/apache2/sites-available/000-default.conf
		and in tag <Directory /var/www/html/> add
		DirectoryIndex index.php
		
> A virtual host named protected.ceedordinary2223.edu
	First I create a folder named Protected and a file protected.html
		sudo mkdir /var/www/html/protected
		sudo nano /var/www/html/protected/protected.html
	> showing a page with the message “Get out of Here!”:
	sudo nano /var/www/html/protected/protected.html

> only reachable connecting from the host machine IP and protected by Basic
Authentication, only accepting user YourSurname.
	I create my username 
	sudo htpasswd -c /etc/apache2/passwd evadiaz
New password: 1234
Re-type new password: 
Adding password for user evadiaz
And now we are going to create configuration file for our virtual host:
sudo nano /etc/apache2/sites-available/protected.conf
        <Directory /var/www/html/protected>
                DirectoryIndex protected.php
                Options Indexes FollowSymLinks Multiviews
                AllowOverride None
                Order allow,deny
                Allow from 10.0.2.5
                AuthType Basic
                AuthUserFile /etc/apache2/passwd
                Require user evadiaz
        <Directory>



		
		

		
		
